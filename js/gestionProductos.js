var productosObtenidos;
function getProductos() {
	var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
	var request = new XMLHttpRequest();
	request.onreadystatechange = function () {
        if(request.readyState === 4 && request.status === 200) {
            //sssssssssssconsole.log(request.responseText);
            productosObtenidos = request.responseText;
            procesarProductos();
        }
    }
    request.open("get", url, true);
    request.send();
}

function procesarProductos(){
	var jsonProductos = JSON.parse(productosObtenidos);
	for (var i=0; i < jsonProductos.value.length; i++){
		console.log(jsonProductos.value[i].ProductName);
	}
	var divTabla = document.getElementById("divTabla") 
	var tabla = document.createElement("table");
	var tbody = document.createElement("tbody");
	tabla.classList.add("table");
	tabla.classList.add("table-striped");
	var nuevaFila = document.createElement("tr");
	var nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "ProductName";
	nuevaFila.append(nuevaColumna);
	tabla.appendChild(nuevaFila);
	nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "UnitPrice";
	nuevaFila.append(nuevaColumna);
	tabla.appendChild(nuevaFila);
	nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "UnitInStock";
	nuevaFila.append(nuevaColumna);
	
	tabla.appendChild(nuevaFila);
	for (var i=0; i < jsonProductos.value.length; i++){
		var nuevaFila = document.createElement("tr");
		var nuevaColumna = document.createElement("td");
		nuevaColumna.innerText = jsonProductos.value[i].ProductName;
		nuevaFila.append(nuevaColumna);
		nuevaColumna = document.createElement("td");
		nuevaColumna.innerText = jsonProductos.value[i].UnitPrice;
		nuevaFila.append(nuevaColumna);
		nuevaColumna = document.createElement("td");
		nuevaColumna.innerText = jsonProductos.value[i].UnitsInStock;
		nuevaFila.append(nuevaColumna);
		tbody.appendChild(nuevaFila);
		tabla.appendChild(tbody);
	}
	divTabla.append(tabla);
	;//alert(jsonProductos.value[0].ProductName)
}

