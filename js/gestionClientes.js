var clientesObtenidos;
function getClientes() {
	var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
	var request = new XMLHttpRequest();
	request.onreadystatechange = function () {
        if(request.readyState === 4 && request.status === 200) {
            //console.log(request.responseText);
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }
    request.open("get", url, true);
    request.send();
}

function procesarClientes(){
	var pathFlag = "http://img.freeflagicons.com/thumb/flag_background";

	//<img src="http://img.freeflagicons.com/thumb/flag_background/afghanistan/afghanistan_640.png" width="30" height="20" alt="Flag background. Download flag icon of Afghanistan at PNG format">
	var jsonClientes = JSON.parse(clientesObtenidos);
	for (var i=0; i < jsonClientes.value.length; i++){
		console.log(jsonClientes.value[i].ContactName);
	}

	var divTabla = document.getElementById("divClientes") 
	var tabla = document.createElement("table");
	var tbody = document.createElement("tbody");
	tabla.classList.add("table");
	tabla.classList.add("table-striped");
	var nuevaFila = document.createElement("tr");
	var nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "ContactName";
	nuevaFila.append(nuevaColumna);
	tabla.appendChild(nuevaFila);
	nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "City";
	nuevaFila.append(nuevaColumna);
	tabla.appendChild(nuevaFila);
	nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "Phone";
	nuevaFila.append(nuevaColumna);
	nuevaColumna = document.createElement("th");
	nuevaColumna.innerText = "Country";
	nuevaFila.append(nuevaColumna);
	
	tabla.appendChild(tbody);
	for (var i=0; i < jsonClientes.value.length; i++){
		var nuevaFila = document.createElement("tr");
		var nuevaColumna = document.createElement("td");
		nuevaColumna.innerText = jsonClientes.value[i].ContactName;
		nuevaFila.append(nuevaColumna);
		nuevaColumna = document.createElement("td");
		nuevaColumna.innerText = jsonClientes.value[i].City;
		nuevaFila.append(nuevaColumna);
		nuevaColumna = document.createElement("td");
		nuevaColumna.innerText = jsonClientes.value[i].Phone;
		nuevaFila.append(nuevaColumna);
		nuevaColumna = document.createElement("td");
		var country = jsonClientes.value[i].Country;
		if (country === 'UK'){
			country = 'united_kingdom';
		}
		var imageFlag = pathFlag + "/" + country.toLowerCase() + "/" + country.toLowerCase() + "_640.png";
		var image = document.createElement("img");
		image.classList.add("flag");
		image.src = imageFlag;
		nuevaColumna.appendChild(image);
		nuevaFila.append(nuevaColumna);
		tbody.appendChild(nuevaFila);
		tabla.appendChild(tbody);
	}
	divTabla.append(tabla);
	;//alert(jsonProductos.value[0].ProductName)
}

